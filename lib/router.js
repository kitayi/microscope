var requireLogin = function () {
  if (!Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      this.render("accessDenied");
    }
  } else {
    this.next();
  }
}

Router.configure({
  layoutTemplate: "layout",
  loadingTemplate: "loading",
  notFoundTemplate: "notFound",
  waitOn: function () {
    return Meteor.subscribe("notifications");
  }
});


Router.route("/posts/:_id", {
  name: "postDetail",
  waitOn: function () {
    return [
      Meteor.subscribe("singlePost", this.params._id),
      Meteor.subscribe("comments", this.params._id)
    ];
  },
  data: function () { 
    return Posts.findOne(this.params._id); 
  }
});

Router.route("/posts/:_id/edit", {
  name: "postEdit",
  waitOn: function(){
    return Meteor.subscribe("singlePost", this.params._id);
  },
  data: function () {
    return Posts.findOne(this.params._id);
  }
});

Router.route("/submit", {
  name: "postSubmit"
});

PostListController = RouteController.extend({
  template: "postList",
  increment: 5,
  postLimit: function(){
    return parseInt(this.params.postLimit) || this.increment;
  },
  findOptions: function(){
    return {sort: this.sort, limit: this.postLimit()};
  },
  subscriptions: function(){
    this.postSub = Meteor.subscribe("posts", this.findOptions());
  },
  posts: function(){
    return Posts.find({}, this.findOptions());
  },
  data: function(){
    var hasMore = this.posts().count() === this.postLimit();
    
    return {
      posts: this.posts(),
      ready: this.postSub.ready,
      nextPath: hasMore ? this.nextPath() : null
    };
  }
}); 

NewPostsController = PostListController.extend({
  sort: {submitted: -1, _id: -1},
  nextPath: function(){
    return Router.routes.newPosts.path({postLimit: this.postLimit() + this.increment});
  }
});

BestPostsController = PostListController = PostListController.extend({
  sort: {votes: -1, submitted: -1, _id: -1},
  nextPath: function(){
    return Router.routes.bestPosts.path({postLimit: this.postLimit() + this.increment});
  }
});

Router.route("/", {
  name: "home",
  controller: "NewPostsController"
});

Router.route("/new/:postLimit?", {
  name: "newPosts"
});

Router.route("/best/:postLimit?", {
  name: "bestPosts"
});

Router.route("/feed.xml", {
  where: "server",
  name: "rss",
  action: function(){
    var feed = new RSS({
      title: "New Microscope Posts",
      description: "The latest posts from Microscope, the smallest news aggregator."
    });
    
    Posts.find({}, {sort: {submitted: -1}, limit: 20}).forEach(function(post){
      feed.item({
        title: post.title,
        description: post.body,
        author: post.author,
        date: post.submitted,
        url: "/posts/" + post._id
      });
    });
    
    this.response.write(feed.xml());
    this.response.end();
  }
});

Router.route("/api/posts", {
  where: "server",
  name: "apiPosts",
  action: function(){
    var parameters = this.request.query;
    var limit = !!parameters.limit ? parseInt(parameters.limit) : 20;
    var data = Posts.find({}, {
      limit: limit, 
      fields: {title: 1, author: 1, url: 1, submitted: 1}
    }).fetch();
    
    this.response.write(JSON.stringify(data));
    this.response.end();
  }
});

Router.route("/api/posts/:_id", {
  where: "server",
  name: "apiPost",
  action: function(){
    var post = Posts.findOne(this.params._id);
    
    if (post){
      this.response.write(JSON.stringify(post));
    } else {
      this.response.write("Post not found.");
    }
    this.response.end();
  }
});

if (Meteor.isClient){
  // Show the "not found" template for falsy post pages (null, false, undefined, empty)
  Router.onBeforeAction("dataNotFound", { only: "postDetail" });
  Router.onBeforeAction(requireLogin, { only: "postSubmit" });
}

