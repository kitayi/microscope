// publishes all posts for all users
Meteor.publish("posts", function(options){
  check(options, {
    sort: Object,
    limit: Number
  });
  return Posts.find({}, options);
});

Meteor.publish("singlePost", function(id){
  check(id, String);
  return Posts.find(id);
});

// publishes only comments relevant to the currently viewed post
Meteor.publish("comments", function(postId){
  check(postId, String);
  return Comments.find({postId: postId});
});

// publishes only notifications relevant to the current user that haven't been read yet
Meteor.publish("notifications", function(){
  return Notifications.find({userId: this.userId, read: false});
});