/////////////////////////////////////////////////////////////////////////
// Fixture Data
/////////////////////////////////////////////////////////////////////////

if (Posts.find().count() === 0) {
  var now = new Date().getTime();
  
  // create two users
  var userOneId = Meteor.users.insert({
    profile: { name: "Tom Coleman" }
  });

  var userTwoId = Meteor.users.insert({
    profile: { name: "Sacha Greif" }
  });

  var userOne = Meteor.users.findOne(userOneId);
  var userTwo = Meteor.users.findOne(userTwoId);

  var telescopeId = Posts.insert({
    title: "Introducing Telescope",
    userId: userTwo._id,
    author: userTwo.profile.name,
    url: "http://sachagreif.com/introducing-telescope/",
    submitted: new Date(now - 7 * 3600 * 1000),
    commentsCount: 2,
    upvoters: [],
    votes: 0
  });

  Comments.insert({
    postId: telescopeId,
    userId: userOne._id,
    author: userOne.profile.name,
    submitted: new Date(now - 5 * 3600 * 1000),
    body: "Interesting project Sacha, can I get involved?"
  });

  Comments.insert({
    postId: telescopeId,
    userId: userTwo._id,
    author: userTwo.profile.name,
    submitted: new Date(now - 3 * 3600 * 1000),
    body: "You sure can, Tom!"
  });

  Posts.insert({
    title: "Meteor",
    userId: userOne._id,
    author: userOne.profile.name,
    url: "http://meteor.com",
    submitted: new Date(now - 10 * 3600 * 1000),
    commentsCount: 0,
    upvoters: [],
    votes: 0
  });

  Posts.insert({
    title: "The Meteor Book",
    userId: userOne._id,
    author: userOne.profile.name,
    url: "http://themeteorbook.com",
    submitted: new Date(now - 12 * 3600 * 1000),
    commentsCount: 0,
    upvoters: [],
    votes: 0
  });
  
  for (var i = 0; i < 10; i++){
    Posts.insert({
      title: "Test post #" + i,
      author: userOne.profile.name,
      userId: userOne._id,
      url: "http://google.com/?q=test-" + i,
      submitted: new Date(now - i * 3600 * 1000),
      commentsCount: 0,
      upvoters: [],
      votes: 0
    });
  }
}