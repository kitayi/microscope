///////////////////////////////////////////////////////////////////////
// onCreated
///////////////////////////////////////////////////////////////////////

Template.postSubmit.onCreated(function(){
  // initialize error object in session
  Session.set("postSubmitErrors", {});
});

///////////////////////////////////////////////////////////////////////
// helpers
///////////////////////////////////////////////////////////////////////

Template.postSubmit.helpers({
  // returns error message
  errorMessage: function(field){
    return Session.get("postSubmitErrors")[field];
  },
  // checks for the presence of a message and returns has-error if one exists
  errorClass: function(field){
    return !!Session.get("postSubmitErrors")[field] ? "has-error" : "";
  }
});

///////////////////////////////////////////////////////////////////////
// events
///////////////////////////////////////////////////////////////////////

Template.postSubmit.events({
  "submit form": function(event){
    event.preventDefault();
    
    var post = {
      url: $(event.target).find("[name=url]").val(),
      title: $(event.target).find("[name=title]").val()
    };
    
    var errors = validatePost(post);
    if (errors.title || errors.url){
      return Session.set("postSubmitErrors", errors);
    }
    
    Meteor.call("postInsert", post, function(error, result){
      // display the error to the user and abort if failure
      if (error){
        return throwError(error.reason);
      }
      // show this result but route anyways
      if (result.postExists){
        throwError("This link has already been posted.");
      }
      Router.go("postDetail", {_id: result._id});
    });
  }
});