///////////////////////////////////////////////////////////////////////
// onCreated
///////////////////////////////////////////////////////////////////////

Template.postEdit.onCreated(function(){
  // Initialize an empty session object upon loading the template
  Session.set("postEditErrors", {});
});

///////////////////////////////////////////////////////////////////////
// helpers
///////////////////////////////////////////////////////////////////////

Template.postEdit.helpers({
  // returns error message
  errorMessage: function(field){
    return Session.get("postEditErrors")[field];
  },
  // checks for the presence of a message and returns has-error if one exists
  errorClass: function(field){
    return !!Session.get("postEditErrors")[field] ? "has-error" : "";
  }
});

///////////////////////////////////////////////////////////////////////
// events
///////////////////////////////////////////////////////////////////////

Template.postEdit.events({
  "submit form": function(event){
    event.preventDefault();
    
    var currentPostId = this._id;
    
    var postProperties = {
      url: $(event.target).find("[name=url]").val(),
      title: $(event.target).find("[name=title]1l").val()
    }
    
    var errors = validatePost(postProperties);
    if (errors.title || errors.url){
      return Session.set("postEditErrors", errors);
    }
    
    Posts.update(currentPostId, {$set: postProperties}, function(error){
      if (error){
        // display the error to the user
        throwError(error.reason);
      } else {
        Router.go("postDetail", {_id: currentPostId});
      }
    });
  },
  
  "click .delete": function(event){
    event.preventDefault();
    
    if (confirm("Delete this post?")){
      var currentPostId = this._id;
      Posts.remove(currentPostId);
      Router.go("home");
    }
  }
});