Template.registerHelper("pluralize", function(number, thing){
  // fairly stupid pluralizer
  if (number === 1){
    return "1 " + thing;
  } else {
    return number + " " + thing + "s";
  }
});